package com.example.onstart;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onStart(){
        super.onStart();

        Log.i("cliclo_de_vida", "OnStart");
        Toast.makeText(this, "OnStart",Toast.LENGTH_LONG).show();
    }


    public void onResume(){
        super.onResume();

        Log.i("cliclo_de_vida", "OnResume");
        Toast.makeText(this, "OnResume",Toast.LENGTH_LONG).show();
    }


    public void onDestroy() {
        super.onDestroy();

        Log.i("cliclo_de_vida", "OnDestroy");
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_LONG).show();
    }
    public void onPause() {
        super.onPause();

        Log.i("cliclo_de_vida", "OnPause");
        Toast.makeText(this, "OnPause", Toast.LENGTH_LONG).show();
    }

    }

